import boto3

def lambda_handler(event, context):
    ec2_conn = boto3.client('ec2', region_name='eu-north-1')
    ami_json = ec2_conn.describe_images(Owners=['self'])
    print(ami_json["Images"])
    print(type(ami_json["Images"]))
    
    for i in ami_json["Images"]:
        print(i["BlockDeviceMappings"][0]["Ebs"]["SnapshotId"])
        
        ec2_conn.deregister_image(ImageId=i["ImageId"])
        ec2_conn.delete_snapshot(SnapshotId=i["BlockDeviceMappings"][0]["Ebs"]["SnapshotId"])
        
    return {"statusCode":200, "body": "AMI's and snapshots removed"}