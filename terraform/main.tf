# Terraform config
terraform {
  required_version = "~>1.1.8"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.7.0"
    }

}

  backend "s3" {
    bucket = "statefiles-bucket"
    key    = "terraform_013.tfstate"
    region = "eu-north-1"
    dynamodb_table = "013-state"
    
  }
}

# AWS provider config
provider "aws" {
  region = "eu-north-1"
}

# VPC
module "vpc" {
  source = "git@gitlab.com:patrykpanter/multirepo-vpc.git?ref=tags/v0.0.1"
  cidr = var.vpc.cidr
  vpc_name_prefix = var.vpc.name_prefix
  subnets_map = var.vpc.subnets
}

# Security groups
module "security_group" {
  source = "./modules/security_group"
  security_groups = var.security_groups
  vpc_id = module.vpc.vpc_id
}

module "ec2" {
  source = "git@gitlab.com:patrykpanter/multirepo-ec2.git?ref=tags/v0.0.1"
  ec2s_map = var.ec2s
  subnet_ids_map = module.vpc.subnet_ids
  security_group_ids_map = module.security_group.security_group_ids
  ebs_instance = var.ebs_instance
}

module "lb" {
  source = "git@gitlab.com:patrykpanter/multirepo-lb.git?ref=tags/v0.0.1"
  name_prefix = var.lb.name_prefix
  private_port = var.lb.private_port
  public_port = var.lb.public_port
  load_balancer_type = var.lb.load_balancer_type
  log_bucket = var.lb.log_bucket
  vpc_id = module.vpc.vpc_id
  subnets = [for subnet in var.lb.subnets: module.vpc.subnet_ids[subnet]]
  security_groups = [for group in var.lb.security_groups: module.security_group.security_group_ids[group]]
  tarrget_instance_id = module.ec2.ec2s[var.lb.target_instance].id
}

module "s3" {
  source = "git@gitlab.com:patrykpanter/multirepo-s3.git?ref=tags/v0.0.1"
  s3_website_bucket_name = var.s3_bucket_name
}


### Lambda ###

locals {
  lambda_zip_file = "packages/lambda_function.zip"
  lambda_handler_file_name = "lambda_function"
  lambda_handler_function_name = "lambda_handler"
  lambda_myregion = "eu-north-1"
  lambda_owner_id= "699942661490"
}

data "archive_file" "lambda_src_code" {
  type = "zip"
  source_file = "${local.lambda_handler_file_name}.py"
  output_path = "${local.lambda_zip_file}"
}

resource "aws_lambda_function" "lambda_function" {
  filename = "${local.lambda_zip_file}"
  function_name = "${local.lambda_handler_file_name}"
  role = aws_iam_role.iam_for_lambda.arn
  handler = "${local.lambda_handler_file_name}.${local.lambda_handler_function_name}"

  source_code_hash = "filebase64sha256(${local.lambda_zip_file})"

  runtime = "python3.9"
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "policy" {
  name        = "test-policy"
  description = "A test policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "deleteImageAndSnapshot",
            "Effect": "Allow",
            "Action": [
                "ec2:DeregisterImage",
                "ec2:DeleteSnapshot"
            ],
            "Resource": [
                "arn:aws:ec2:*::snapshot/*",
                "arn:aws:ec2:*::image/*"
            ]
        },
        {
            "Sid": "DescribeImagesSnapshots",
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeImages",
                "ec2:DescribeSnapshots"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${local.lambda_myregion}:${local.lambda_owner_id}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.resource.path}"
}

### API Gateway ###

resource "aws_api_gateway_rest_api" "api" {
  name = "api-gateway-rest"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "resource" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "resource"
}

resource "aws_api_gateway_method" "method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "GET"
  authorization = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.resource.id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_function.invoke_arn
}

resource "aws_api_gateway_deployment" "deployment" {
  rest_api_id = aws_api_gateway_rest_api.api.id

  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.resource.id,
      aws_api_gateway_method.method.id,
      aws_api_gateway_integration.integration.id,
    ]))
  }  

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_usage_plan" "usage_plan" {
  name = "usage-plan"

  api_stages {
    api_id = aws_api_gateway_rest_api.api.id
    stage  = aws_api_gateway_stage.stage.stage_name
  }
}

resource "aws_api_gateway_api_key" "api_key" {
  name = "api-key"
}

resource "aws_api_gateway_usage_plan_key" "usage_plan_key" {
  key_id        = aws_api_gateway_api_key.api_key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.usage_plan.id
}

resource "aws_api_gateway_stage" "stage" {
  deployment_id = aws_api_gateway_deployment.deployment.id
  rest_api_id   = aws_api_gateway_rest_api.api.id
  stage_name    = "stage"
}